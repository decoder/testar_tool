package io.swagger.api;

import io.swagger.model.Settings;
import io.swagger.model.TestarResponse;
import io.swagger.service.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-11-06T10:40:54.389+01:00[Europe/Paris]")
@Controller
public class TestarApiController implements TestarApi {

	private static final Logger log = LoggerFactory.getLogger(TestarApiController.class);

	@SuppressWarnings("unused")
	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@Autowired
	ITestarService testarService;
	@Autowired
	IHtmlReportService htmlReportService;
	@Autowired
	IAnalysisService analysisService;


	@org.springframework.beans.factory.annotation.Autowired
	public TestarApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	public ResponseEntity<Object> testarPost(@ApiParam(value = "Send the desired parameters to launch TESTAR through command line, connect with the SUT, generate the Artefacts and feed the PKM using the PKM-API with authentication." ,required=true )  @Valid @RequestBody Settings body
			) {
		String accept = request.getHeader("Accept");
		if (accept != null && accept.contains("application/json")) {
			try {
				return new ResponseEntity<Object>(testarService.execute(body), HttpStatus.OK);
			} catch (SettingsException se) {
				log.error(se.getMessage());
				return new ResponseEntity<Object>(se.getInvalidSetting(), HttpStatus.BAD_REQUEST);
			} catch (OrientDBException odbe) {
				log.error(odbe.getMessage());
				return new ResponseEntity<Object>(odbe.getInvalidOrientDBCredentials(), HttpStatus.UNAUTHORIZED);
			} catch (TestarExecutionException tee) {
				log.error(tee.getMessage());
				return new ResponseEntity<Object>(tee.getTestarExceptionMessage(), HttpStatus.NOT_FOUND);
			} catch (IOException e) {
				log.error("Couldn't serialize response for content type application/json", e);
				return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
			} catch (Exception e) {
				log.error("Internal error", e);
				return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		return new ResponseEntity<Object>(HttpStatus.NOT_IMPLEMENTED);
	}

	public ResponseEntity<Void> testarReportArtefactIdGet(@ApiParam(value = "Indicate the ArtefactId of the Test Results reports.",required=true) @PathVariable("artefactId") String artefactId
			) {
		String accept = request.getHeader("Accept");
		if(accept != null) {
			try {
				return new ResponseEntity<Void>(htmlReportService.htmlReport(artefactId), HttpStatus.OK);
			} catch (Exception e) {
				log.error("Internal error", e);
				return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
	}

	public ResponseEntity<Void> testarAnalysisOrientDBnameGet(@ApiParam(value = "Indicate the OrientDB database name.",required=true) @PathVariable("OrientDBname") String orientDBname
			,@ApiParam(value = "Access key to the PKM" ,required=true) @RequestHeader(value="key", required=true) String key
			) {
		String accept = request.getHeader("Accept");
		String referer = request.getHeader("Referer");
		if(accept != null) {
			try {
				return new ResponseEntity<Void>(analysisService.analysis(orientDBname, key, referer), HttpStatus.OK);
			} catch (TestarExecutionException tee) {
				log.error(tee.getMessage());
				return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
			} catch (Exception e) {
				log.error("Internal error", e);
				return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
	}
}
