/***************************************************************************************************
 *
 * Copyright (c) 2020 Universitat Politecnica de Valencia - www.upv.es
 * Copyright (c) 2020 Open Universiteit - www.ou.nl
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************************************/

package io.swagger.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.swagger.api.OrientDBException;
import io.swagger.api.SettingsException;
import io.swagger.api.TestarExecutionException;
import io.swagger.model.Settings;
import io.swagger.model.TestarResponse;

@Service
public class TestarService implements ITestarService {

	private static final Logger log = LoggerFactory.getLogger(TestarService.class);

	char dq = '"';

	@Override
	public TestarResponse execute(Settings settings) throws Exception {

		TestarResponse testarResponse = new TestarResponse();

		List<String> commands = new ArrayList<>();
		commands.add("testar.bat");

		// TESTAR Java protocol selection.
		if (StringUtils.isNotEmpty(settings.getSse())) {
			commands.add("sse="+settings.getSse());
		}

		// Always disable TESTAR GUI
		commands.add("ShowVisualSettingsDialogOnStartup=false");

		// process the SUTConnectorValue information
		if(StringUtils.isNotEmpty(settings.getSuTConnectorValue())) {
			if(settings.getSuTConnector().equals(Settings.SuTConnectorEnum.WEB_DRIVER)) {
				commands.add("SUTConnectorValue=" + prepareWebdriverConnector(settings.getSuTConnectorValue()));
			} else {
				commands.add("SUTConnectorValue=" + prepareParam(settings.getSuTConnectorValue()));
			}
		}

		// Prepare all possible command inputs
		prepareSettingsCommands(commands, settings);

		String[] commadsArray = commands.toArray(new String[0]);
		StringBuilder cmdToExecute = new StringBuilder();

		for (String string : commadsArray) {
			log.info(string);
			cmdToExecute.append(string + " ");
		}
		
		System.out.println(cmdToExecute);

		StringBuilder executionOutput = new StringBuilder("");
		StringBuilder executionError = new StringBuilder("");

		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", cmdToExecute.toString());
		Process p = builder.start();

		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

		String s = null;
		while ((s = stdInput.readLine()) != null) {
			System.out.println(s);
			executionOutput.append(s);
		}

		while ((s = stdError.readLine()) != null) {
			System.err.println(s);
			executionError.append(s);
		}
		
		if(!executionError.toString().isEmpty()) {
			if(executionError.toString().contains("Error trying to connect with OrientDB database")) {
				throw new OrientDBException(executionError.toString(), executionError.toString());
			}
			if(executionError.toString().matches("error.*Error.*ERROR.*exception.*Exception.*EXCEPTION")){
				throw new TestarExecutionException(executionError.toString(), executionError.toString());
			}
			if(executionError.toString().contains("Not valid key or not found project in the database")) {
				throw new TestarExecutionException("Not valid key or project", "Not valid key or project");
			}
		}

		/** PKM-API prints {"TESTARTestResults artefactId":""} */
		String testResultsArtefactId = "TESTARTestResults artefactId: ";
		if(executionOutput.toString().contains("TESTARTestResults artefactId\":\"")) {
			testResultsArtefactId = testResultsArtefactId.concat(substringArtefactId(executionOutput.toString(), "TESTARTestResults artefactId\":\""));
		}

		/** PKM-API prints {"TESTARStateModels artefactId":""} */
		String stateModelArtefactId = "TESTARStateModels artefactId: ";
		if(executionOutput.toString().contains("TESTARStateModels artefactId\":\"")) {
			stateModelArtefactId = stateModelArtefactId.concat(substringArtefactId(executionOutput.toString(), "TESTARStateModels artefactId\":\""));
		}

		testarResponse.addArtefactIdItem(testResultsArtefactId);
		testarResponse.addArtefactIdItem(stateModelArtefactId);

		return testarResponse;
	}

	/**
	 * Get POST setting and modify into a valid String for command line.
	 * 
	 * @param param
	 * @return string parameter
	 */
	private String prepareParam(Object param) {
		String parameter = castParameterValue(param);
		parameter = parameter.replace(File.separator, File.separator + File.separator);
		parameter = dq + parameter + dq;
		return parameter;
	}

	/**
	 * Prepare the String to be used as WebdriverConnector command line value.
	 * SUTConnectorValue=" ""C:\\webdrivers\\chromedriver.exe"" ""1920x900+0+0"" ""https://www.ou.nl"" "
	 * 
	 * @param sutConnectorValue
	 * @return string WebdriverConnector
	 */
	private String prepareWebdriverConnector(String sutConnectorValue) {
		return dq + " " + dq + dq + "C:\\\\webdrivers\\\\chromedriver.exe" + dq + dq +
				" " + dq + dq + "1920x900+0+0" + dq + dq +
				" " + dq + dq + sutConnectorValue + dq + dq + " " + dq;
	}

	private String castParameterValue(Object param) {
		if(param instanceof Boolean) {
			return Boolean.toString(((Boolean) param).booleanValue());
		}
		else if (param instanceof Integer) {
			return Integer.toString(((Integer) param).intValue());
		}
		else if (param instanceof Double) {
			return Double.toString(((Double) param).doubleValue());
		}

		return param.toString();
	}

	/**
	 * With the TESTAR output message, obtain the Artefact Identifier of desired Artefact Name.
	 * 
	 * @param executionOutput
	 * @param find
	 * @return artefactId
	 */
	private String substringArtefactId(String executionOutput, String find) {
		String artefactId = "ERROR";
		String pkmOutputInfo = executionOutput.substring(executionOutput.indexOf(find) + find.length());
		artefactId = StringUtils.split(pkmOutputInfo, "\"}")[0];
		artefactId = artefactId.replace("\"}","").replace("\n", "").replace("\r", "");
		artefactId = artefactId.trim();

		return artefactId;
	}
	
	/**
	 * Prepare all possible Settings Commands
	 */
	private void prepareSettingsCommands(List<String> commands, Settings settings) throws SettingsException {
		/**
		 * Obtain all GET and is boolean methods from settings object
		 * to automatically prepare the commands arguments.
		 * https://stackoverflow.com/questions/11224217
		 * https://stackoverflow.com/questions/38017882
		 * 
		 * However this implementation requires a modification of Setting.java method names
		 * something that changes the Swagger annotation and content of examples.
		 */
		/** for (Method m : settings.getClass().getMethods()) {
			// Exclude getSse() and getSUTConnectorValue(), because were configured previously
			if (m.getName().startsWith("get") && m.getParameterTypes().length == 0 
					&& !m.getName().startsWith("getClass")
					&& !m.getName().contains("Sse") && !m.getName().contains("SUTConnectorValue")
					&& StringUtils.isNotEmpty(m.invoke(settings).toString())) {

				String paramName = m.getName().substring(m.getName().indexOf("get") + 3);
				commands.add(paramName + "=" + prepareParam(m.invoke(settings)));
			}
			else if (m.getName().startsWith("is") && m.getParameterTypes().length == 0 
					&& StringUtils.isNotEmpty(m.invoke(settings).toString())) {

				String paramName = m.getName().substring(m.getName().indexOf("is") + 2);
				commands.add(paramName + "=" + prepareParam(m.invoke(settings)));
			}
		}**/
		
		if(checkSetting("Mode", settings.getMode())) {
			commands.add("Mode=" + prepareParam(settings.getMode().toString()));
		}
		if(checkSetting("AccessBridgeEnabled", settings.isAccessBridgeEnabled())) {
			commands.add("AccessBridgeEnabled=" + prepareParam(settings.isAccessBridgeEnabled().toString()));
		}
		if(checkSetting("SUTConnector", settings.getSuTConnector())) {
			commands.add("SUTConnector=" + prepareParam(settings.getSuTConnector().toString()));
		}
		if(checkSetting("Sequences", settings.getSequences())) {
			commands.add("Sequences=" + prepareParam(settings.getSequences().toString()));
		}
		if(checkSetting("SequenceLength", settings.getSequenceLength())) {
			commands.add("SequenceLength=" + prepareParam(settings.getSequenceLength().toString()));
		}
		if(checkSetting("SuspiciousTitles", settings.getSuspiciousTitles())) {
			commands.add("SuspiciousTitles=" + prepareParam(settings.getSuspiciousTitles()));
		}
		if(checkSetting("TimeToFreeze", settings.getTimeToFreeze())) {
			commands.add("TimeToFreeze=" + prepareParam(settings.getTimeToFreeze().toString()));
		}
		if(checkSetting("ProcessListenerEnabled", settings.isProcessListenerEnabled())) {
			commands.add("ProcessListenerEnabled=" + prepareParam(settings.isProcessListenerEnabled().toString()));
		}
		if(checkSetting("SuspiciousProcessOutput", settings.getSuspiciousProcessOutput())) {
			commands.add("SuspiciousProcessOutput=" + prepareParam(settings.getSuspiciousProcessOutput()));
		}
		if(checkSetting("ProcessLogs", settings.getProcessLogs())) {
			commands.add("ProcessLogs=" + prepareParam(settings.getProcessLogs()));
		}
		if(checkSetting("ClickFilter", settings.getClickFilter())) {
			commands.add("ClickFilter=" + prepareParam(settings.getClickFilter()));
		}
		if(checkSetting("ForceForeground", settings.isForceForeground())) {
			commands.add("ForceForeground=" + prepareParam(settings.isForceForeground().toString()));
		}
		if(checkSetting("ActionDuration", settings.getActionDuration())) {
			commands.add("ActionDuration=" + prepareParam(settings.getActionDuration().toString()));
		}
		if(checkSetting("TimeToWaitAfterAction", settings.getTimeToWaitAfterAction())) {
			commands.add("TimeToWaitAfterAction=" + prepareParam(settings.getTimeToWaitAfterAction().toString()));
		}
		if(checkSetting("MaxTime", settings.getMaxTime())) {
			commands.add("MaxTime=" + prepareParam(settings.getMaxTime().toString()));
		}
		if(checkSetting("StartupTime", settings.getStartupTime())) {
			commands.add("StartupTime=" + prepareParam(settings.getStartupTime().toString()));
		}
		if(checkSetting("MaxReward", settings.getMaxReward())) {
			commands.add("MaxReward=" + prepareParam(settings.getMaxReward().toString()));
		}
		if(checkSetting("Discount", settings.getDiscount())) {
			commands.add("Discount=" + prepareParam(settings.getDiscount().toString()));
		}
		if(checkSetting("AlwaysCompile", settings.isAlwaysCompile())) {
			commands.add("AlwaysCompile=" + prepareParam(settings.isAlwaysCompile().toString()));
		}
		if(checkSetting("OnlySaveFaultySequences", settings.isOnlySaveFaultySequences())) {
			commands.add("OnlySaveFaultySequences=" + prepareParam(settings.isOnlySaveFaultySequences().toString()));
		}
		if(checkSetting("StopGenerationOnFault", settings.isStopGenerationOnFault())) {
			commands.add("StopGenerationOnFault=" + prepareParam(settings.isStopGenerationOnFault().toString()));
		}
		if(checkSetting("ProcessesToKillDuringTest", settings.getProcessesToKillDuringTest())) {
			commands.add("ProcessesToKillDuringTest=" + prepareParam(settings.getProcessesToKillDuringTest()));
		}
		if(checkSetting("PathToReplaySequence", settings.getPathToReplaySequence())) {
			commands.add("PathToReplaySequence=" + prepareParam(settings.getPathToReplaySequence()));
		}
		if(checkSetting("OutputDir", settings.getOutputDir())) {
			commands.add("OutputDir=" + prepareParam(settings.getOutputDir()));
		}
		if(checkSetting("TempDir", settings.getTempDir())) {
			commands.add("TempDir=" + prepareParam(settings.getTempDir()));
		}
		if(checkSetting("MyClassPath", settings.getMyClassPath())) {
			commands.add("MyClassPath=" + prepareParam(settings.getMyClassPath()));
		}
		if(checkSetting("OverrideWebDriverDisplayScale", settings.getOverrideWebDriverDisplayScale())) {
			commands.add("OverrideWebDriverDisplayScale=" + prepareParam(settings.getOverrideWebDriverDisplayScale().toString()));
		}
		if(checkSetting("StateModelEnabled", settings.isStateModelEnabled())) {
			commands.add("StateModelEnabled=" + prepareParam(settings.isStateModelEnabled().toString()));
		}
		if(checkSetting("DataStore", settings.getDataStore())) {
			commands.add("DataStore=" + prepareParam(settings.getDataStore().toString()));
		}
		if(checkSetting("DataStoreType", settings.getDataStoreType())) {
			commands.add("DataStoreType=" + prepareParam(settings.getDataStoreType().toString()));
		}
		if(checkSetting("DataStoreServer", settings.getDataStoreServer())) {
			commands.add("DataStoreServer=" + prepareParam(settings.getDataStoreServer()));
		}
		if(checkSetting("DataStoreDirectory", settings.getDataStoreDirectory())) {
			commands.add("DataStoreDirectory=" + prepareParam(settings.getDataStoreDirectory()));
		}
		if(checkSetting("DataStoreDB", settings.getDataStoreDB())) {
			commands.add("DataStoreDB=" + prepareParam(settings.getDataStoreDB()));
		}
		if(checkSetting("DataStoreUser", settings.getDataStoreUser())) {
			commands.add("DataStoreUser=" + prepareParam(settings.getDataStoreUser()));
		}
		if(checkSetting("DataStorePassword", settings.getDataStorePassword())) {
			commands.add("DataStorePassword=" + prepareParam(settings.getDataStorePassword()));
		}
		if(checkSetting("DataStoreMode", settings.getDataStoreMode())) {
			commands.add("DataStoreMode=" + prepareParam(settings.getDataStoreMode().toString()));
		}
		if(checkSetting("ResetDataStore", settings.isResetDataStore())) {
			commands.add("ResetDataStore=" + prepareParam(settings.isResetDataStore().toString()));
		}
		if(checkSetting("ActionSelectionAlgorithm", settings.getActionSelectionAlgorithm())) {
			commands.add("ActionSelectionAlgorithm=" + prepareParam(settings.getActionSelectionAlgorithm().toString()));
		}
		if(checkSetting("StateModelStoreWidgets", settings.isStateModelStoreWidgets())) {
			commands.add("StateModelStoreWidgets=" + prepareParam(settings.isStateModelStoreWidgets().toString()));
		}
		if(checkSetting("AbstractStateAttributes", settings.getAbstractStateAttributes())) {
			commands.add("AbstractStateAttributes=" + prepareParam(settings.getAbstractStateAttributes()));
		}
		if(checkSetting("ApplicationName", settings.getApplicationName())) {
			commands.add("ApplicationName=" + prepareParam(settings.getApplicationName()));
		}
		if(checkSetting("ApplicationVersion", settings.getApplicationVersion())) {
			commands.add("ApplicationVersion=" + prepareParam(settings.getApplicationVersion().toString()));
		}
		if(checkSetting("PreviousApplicationName", settings.getPreviousApplicationName())) {
			commands.add("PreviousApplicationName=" + prepareParam(settings.getPreviousApplicationName()));
		}
		if(checkSetting("PreviousApplicationVersion", settings.getPreviousApplicationVersion())) {
			commands.add("PreviousApplicationVersion=" + prepareParam(settings.getPreviousApplicationVersion().toString()));
		}
		if(checkSetting("StateModelDifferenceAutomaticReport", settings.isStateModelDifferenceAutomaticReport())) {
			commands.add("StateModelDifferenceAutomaticReport=" + prepareParam(settings.isStateModelDifferenceAutomaticReport().toString()));
		}
		if(checkSetting("PKMaddress", settings.getPkMaddress())) {
			commands.add("PKMaddress=" + prepareParam(settings.getPkMaddress()));
		}
		if(checkSetting("PKMport", settings.getPkMport())) {
			commands.add("PKMport=" + prepareParam(settings.getPkMport()));
		}
		if(checkSetting("PKMdatabase", settings.getPkMdatabase())) {
			commands.add("PKMdatabase=" + prepareParam(settings.getPkMdatabase()));
		}
		if(checkSetting("PKMusername", settings.getPkMusername())) {
			commands.add("PKMusername=" + prepareParam(settings.getPkMusername()));
		}
		if(checkSetting("PKMkey", settings.getPkMkey())) {
			commands.add("PKMkey=" + prepareParam(settings.getPkMkey()));
		}
	}
	
	/**
	 * Check is TESTAR setting is correct, not null and not empty.
	 * If not throw a setting exception for the API response.
	 */
	private boolean checkSetting(String settingName, Object setting) throws SettingsException {
		if(Objects.isNull(setting)) {
			String message = String.format("This setting is NULL or EMPTY: %s", settingName);
			throw new SettingsException(message, message);
		}
		return !setting.toString().isEmpty();
	}

}
