/***************************************************************************************************
 *
 * Copyright (c) 2020 Universitat Politecnica de Valencia - www.upv.es
 * Copyright (c) 2020 Open Universiteit - www.ou.nl
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************************************/

package io.swagger.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.swagger.api.TestarExecutionException;

@Service
public class AnalysisService implements IAnalysisService{

	private static final Logger log = LoggerFactory.getLogger(HtmlReportService.class);

	@Override
	public Void analysis(String orientDBname, String key, String referer) throws Exception {

		List<String> commands = new ArrayList<>();
		commands.add("testar.bat");

		// TESTAR Report Mode is used to start the web service that allows remote State Model Analysis
		// testar ShowVisualSettingsDialogOnStartup=false Mode=Analysis DataStoreType=remote DataStoreServer=10.0.0.1 DataStoreDB=testar DataStoreUser=testar DataStorePassword=testar
		commands.add("Mode=Analysis");

		// Disable TESTAR GUI
		commands.add("ShowVisualSettingsDialogOnStartup=false");

		commands.add("DataStoreDB=" + orientDBname);

		commands.add("PKMkey=" + key);
		
		commands.add("PKMdatabase=" + getProjectFromReferer(referer));

		String[] commadsArray = commands.toArray(new String[0]);
		StringBuilder cmdToExecute = new StringBuilder();

		for (String string : commadsArray) {
			log.info(string);
			cmdToExecute.append(string + " ");
		}

		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", cmdToExecute.toString());
		Process p = builder.start();

		StringBuilder executionOutput = new StringBuilder("");
		StringBuilder executionError = new StringBuilder("");

		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

		String s = null;
		while ((s = stdInput.readLine()) != null) {
			System.out.println(s);
			executionOutput.append(s);
		}

		while ((s = stdError.readLine()) != null) {
			System.err.println(s);
			executionError.append(s);
		}

		if(!executionError.toString().isEmpty() 
				&& executionError.toString().contains("Not valid key or not found project in the database")) {
			throw new TestarExecutionException("Not valid key or project", "Not valid key or project");
		}

		// Wait until user launchs "ipaddress:8090/shutdown" URL
		p.waitFor();

		return null;
	}

	/**
	 * Extract the DECODER PKM project name from the Referer URL
	 * https://decoder-frontend.ow2.org/home/projects/mythaistar/mythaistar
	 * 
	 * @param referer
	 * @return PKMproject
	 */
	private String getProjectFromReferer(String referer) {
		try {
			return referer.substring(referer.lastIndexOf('/') + 1);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return "Unknown";
	}

}
