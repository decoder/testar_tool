/***************************************************************************************************
 *
 * Copyright (c) 2020 Universitat Politecnica de Valencia - www.upv.es
 * Copyright (c) 2020 Open Universiteit - www.ou.nl
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************************************/


package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TESTAR settings used to select a desired protocol, connect with the desired SUT and customize behaviour decisions.
 */
@ApiModel(description = "TESTAR settings used to select a desired protocol, connect with the desired SUT and customize behaviour decisions.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-11-06T10:40:54.389+01:00[Europe/Paris]")
public class Settings   {
	@JsonProperty("sse")
	private String sse = null;

	@JsonProperty("ShowVisualSettingsDialogOnStartup")
	private Boolean showVisualSettingsDialogOnStartup = null;

	/**
	 * Selects the TESTAR execution mode to Generate test sequences or Replay existing test sequences. Spy the SUT and Record user actions are not enabled in remote API automatic execution.
	 */
	public enum ModeEnum {
		GENERATE("Generate"),

		REPLAY("Replay");

		private String value;

		ModeEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static ModeEnum fromValue(String text) {
			for (ModeEnum b : ModeEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}
	@JsonProperty("Mode")
	private ModeEnum mode = null;

	@JsonProperty("AccessBridgeEnabled")
	private Boolean accessBridgeEnabled = null;

	/**
	 * Indicate how to connect to the SUT. Launch executable file, attach to existing process, attach to existing windows through the Title or use Webdriver.
	 */
	public enum SuTConnectorEnum {
		COMMAND_LINE("COMMAND_LINE"),

		SUT_PROCESS_NAME("SUT_PROCESS_NAME"),

		SUT_WINDOW_TITLE("SUT_WINDOW_TITLE"),

		WEB_DRIVER("WEB_DRIVER");

		private String value;

		SuTConnectorEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static SuTConnectorEnum fromValue(String text) {
			for (SuTConnectorEnum b : SuTConnectorEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}
	@JsonProperty("SUTConnector")
	private SuTConnectorEnum suTConnector = null;

	@JsonProperty("SUTConnectorValue")
	private String suTConnectorValue = null;

	@JsonProperty("Sequences")
	private Integer sequences = null;

	@JsonProperty("SequenceLength")
	private Integer sequenceLength = null;

	@JsonProperty("SuspiciousTitles")
	private String suspiciousTitles = null;

	@JsonProperty("TimeToFreeze")
	private BigDecimal timeToFreeze = null;

	@JsonProperty("ProcessListenerEnabled")
	private Boolean processListenerEnabled = null;

	@JsonProperty("SuspiciousProcessOutput")
	private String suspiciousProcessOutput = null;

	@JsonProperty("ProcessLogs")
	private String processLogs = null;

	@JsonProperty("ClickFilter")
	private String clickFilter = null;

	@JsonProperty("ForceForeground")
	private Boolean forceForeground = null;

	@JsonProperty("ActionDuration")
	private BigDecimal actionDuration = null;

	@JsonProperty("TimeToWaitAfterAction")
	private BigDecimal timeToWaitAfterAction = null;

	@JsonProperty("MaxTime")
	private BigDecimal maxTime = null;

	@JsonProperty("StartupTime")
	private BigDecimal startupTime = null;

	@JsonProperty("MaxReward")
	private BigDecimal maxReward = null;

	@JsonProperty("Discount")
	private BigDecimal discount = null;

	@JsonProperty("AlwaysCompile")
	private Boolean alwaysCompile = null;

	@JsonProperty("OnlySaveFaultySequences")
	private Boolean onlySaveFaultySequences = null;

	@JsonProperty("StopGenerationOnFault")
	private Boolean stopGenerationOnFault = null;

	@JsonProperty("ProcessesToKillDuringTest")
	private String processesToKillDuringTest = null;

	@JsonProperty("PathToReplaySequence")
	private String pathToReplaySequence = null;

	@JsonProperty("OutputDir")
	private String outputDir = null;

	@JsonProperty("TempDir")
	private String tempDir = null;

	@JsonProperty("MyClassPath")
	private String myClassPath = null;

	@JsonProperty("OverrideWebDriverDisplayScale")
	private BigDecimal overrideWebDriverDisplayScale = null;

	@JsonProperty("StateModelEnabled")
	private Boolean stateModelEnabled = null;

	/**
	 * Indicate the graph database where are going to use.
	 */
	public enum DataStoreEnum {
		ORIENTDB("OrientDB");

		private String value;

		DataStoreEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static DataStoreEnum fromValue(String text) {
			for (DataStoreEnum b : DataStoreEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}
	@JsonProperty("DataStore")
	private DataStoreEnum dataStore = null;

	/**
	 * Indicate the type of connection with the graph database. Remote or local.
	 */
	public enum DataStoreTypeEnum {
		REMOTE("remote"),

		PLOCAL("plocal");

		private String value;

		DataStoreTypeEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static DataStoreTypeEnum fromValue(String text) {
			for (DataStoreTypeEnum b : DataStoreTypeEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}
	@JsonProperty("DataStoreType")
	private DataStoreTypeEnum dataStoreType = null;

	@JsonProperty("DataStoreServer")
	private String dataStoreServer = null;

	@JsonProperty("DataStoreDirectory")
	private String dataStoreDirectory = null;

	@JsonProperty("DataStoreDB")
	private String dataStoreDB = null;

	@JsonProperty("DataStoreUser")
	private String dataStoreUser = null;

	@JsonProperty("DataStorePassword")
	private String dataStorePassword = null;

	/**
	 * Select how the data will be stored in the database.
	 */
	public enum DataStoreModeEnum {
		INSTANT("instant"),

		DELAYED("delayed"),

		HYBRID("hybrid"),

		NONE("none");

		private String value;

		DataStoreModeEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static DataStoreModeEnum fromValue(String text) {
			for (DataStoreModeEnum b : DataStoreModeEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}
	@JsonProperty("DataStoreMode")
	private DataStoreModeEnum dataStoreMode = null;

	@JsonProperty("ResetDataStore")
	private Boolean resetDataStore = null;

	/**
	 * State Model action selection algorithm. If model is not deterministic TESTAR will alternate to random selection.
	 */
	public enum ActionSelectionAlgorithmEnum {
		UNVISITED("unvisited"),

		RANDOM("random");

		private String value;

		ActionSelectionAlgorithmEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static ActionSelectionAlgorithmEnum fromValue(String text) {
			for (ActionSelectionAlgorithmEnum b : ActionSelectionAlgorithmEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}
	@JsonProperty("ActionSelectionAlgorithm")
	private ActionSelectionAlgorithmEnum actionSelectionAlgorithm = null;

	@JsonProperty("StateModelStoreWidgets")
	private Boolean stateModelStoreWidgets = null;

	@JsonProperty("AbstractStateAttributes")
	private String abstractStateAttributes = null;

	@JsonProperty("ApplicationName")
	private String applicationName = null;

	@JsonProperty("ApplicationVersion")
	private BigDecimal applicationVersion = null;

	@JsonProperty("PreviousApplicationName")
	private String previousApplicationName = null;

	@JsonProperty("PreviousApplicationVersion")
	private BigDecimal previousApplicationVersion = null;

	@JsonProperty("StateModelDifferenceAutomaticReport")
	private Boolean stateModelDifferenceAutomaticReport = null;

	@JsonProperty("PKMaddress")
	private String pkMaddress = null;

	@JsonProperty("PKMport")
	private String pkMport = null;

	@JsonProperty("PKMdatabase")
	private String pkMdatabase = null;

	@JsonProperty("PKMusername")
	private String pkMusername = null;

	@JsonProperty("PKMkey")
	private String pkMkey = null;

	public Settings sse(String sse) {
		this.sse = sse;
		return this;
	}

	/**
	 * Select the TESTAR Java protocol to execute.
	 * @return sse
	 **/
	@ApiModelProperty(example = "webdriver_MyThaiStar", value = "Select the TESTAR Java protocol to execute.")

	public String getSse() {
		return sse;
	}

	public void setSse(String sse) {
		this.sse = sse;
	}

	public Settings showVisualSettingsDialogOnStartup(Boolean showVisualSettingsDialogOnStartup) {
		this.showVisualSettingsDialogOnStartup = showVisualSettingsDialogOnStartup;
		return this;
	}

	/**
	 * Enable or Disable TESTAR settings GUI. For remote API execution purposes this parameter should be always disables, in other case TESTAR will stay stucked with the GUI opened.
	 * @return showVisualSettingsDialogOnStartup
	 **/
	@ApiModelProperty(example = "false", value = "Enable or Disable TESTAR settings GUI. For remote API execution purposes this parameter should be always disables, in other case TESTAR will stay stucked with the GUI opened.")

	public Boolean isShowVisualSettingsDialogOnStartup() {
		return showVisualSettingsDialogOnStartup;
	}

	public void setShowVisualSettingsDialogOnStartup(Boolean showVisualSettingsDialogOnStartup) {
		this.showVisualSettingsDialogOnStartup = showVisualSettingsDialogOnStartup;
	}

	public Settings mode(ModeEnum mode) {
		this.mode = mode;
		return this;
	}

	/**
	 * Selects the TESTAR execution mode to Generate test sequences or Replay existing test sequences. Spy the SUT and Record user actions are not enabled in remote API automatic execution.
	 * @return mode
	 **/
	@ApiModelProperty(example = "Generate", value = "Selects the TESTAR execution mode to Generate test sequences or Replay existing test sequences. Spy the SUT and Record user actions are not enabled in remote API automatic execution.")

	public ModeEnum getMode() {
		return mode;
	}

	public void setMode(ModeEnum mode) {
		this.mode = mode;
	}

	public Settings accessBridgeEnabled(Boolean accessBridgeEnabled) {
		this.accessBridgeEnabled = accessBridgeEnabled;
		return this;
	}

	/**
	 * Enable AccessBridge technology to create the widget tree for Java Swing applications.
	 * @return accessBridgeEnabled
	 **/
	@ApiModelProperty(example = "false", value = "Enable AccessBridge technology to create the widget tree for Java Swing applications.")

	public Boolean isAccessBridgeEnabled() {
		return accessBridgeEnabled;
	}

	public void setAccessBridgeEnabled(Boolean accessBridgeEnabled) {
		this.accessBridgeEnabled = accessBridgeEnabled;
	}

	public Settings suTConnector(SuTConnectorEnum suTConnector) {
		this.suTConnector = suTConnector;
		return this;
	}

	/**
	 * Indicate how to connect to the SUT. Launch executable file, attach to existing process, attach to existing windows through the Title or use Webdriver.
	 * @return suTConnector
	 **/
	@ApiModelProperty(example = "COMMAND_LINE", value = "Indicate how to connect to the SUT. Launch executable file, attach to existing process, attach to existing windows through the Title or use Webdriver.")

	public SuTConnectorEnum getSuTConnector() {
		return suTConnector;
	}

	public void setSuTConnector(SuTConnectorEnum suTConnector) {
		this.suTConnector = suTConnector;
	}

	public Settings suTConnectorValue(String suTConnectorValue) {
		this.suTConnectorValue = suTConnectorValue;
		return this;
	}

	/**
	 * How to start the SUT through command line, the name of the pocess to connect, the title of the window to attach, or the path of the webdriver and url.
	 * @return suTConnectorValue
	 **/
	@ApiModelProperty(example = "http://10.101.0.243:8081", value = "How to start the SUT through command line, the name of the pocess to connect, the title of the window to attach, or the path of the webdriver and url.")

	public String getSuTConnectorValue() {
		return suTConnectorValue;
	}

	public void setSuTConnectorValue(String suTConnectorValue) {
		this.suTConnectorValue = suTConnectorValue;
	}

	public Settings sequences(Integer sequences) {
		this.sequences = sequences;
		return this;
	}

	/**
	 * Number of sequences that TESTAR will start and close the session connections with the SUT.
	 * minimum: 1
	 * @return sequences
	 **/
	@ApiModelProperty(example = "5", value = "Number of sequences that TESTAR will start and close the session connections with the SUT.")

	@Min(1)  public Integer getSequences() {
		return sequences;
	}

	public void setSequences(Integer sequences) {
		this.sequences = sequences;
	}

	public Settings sequenceLength(Integer sequenceLength) {
		this.sequenceLength = sequenceLength;
		return this;
	}

	/**
	 * Number of actions TESTAR will execute each sequence.
	 * minimum: 1
	 * @return sequenceLength
	 **/
	@ApiModelProperty(example = "100", value = "Number of actions TESTAR will execute each sequence.")

	@Min(1)  public Integer getSequenceLength() {
		return sequenceLength;
	}

	public void setSequenceLength(Integer sequenceLength) {
		this.sequenceLength = sequenceLength;
	}

	public Settings suspiciousTitles(String suspiciousTitles) {
		this.suspiciousTitles = suspiciousTitles;
		return this;
	}

	/**
	 * Regular expression to be applied as Oracles to find suspicious and erroneous messages.
	 * @return suspiciousTitles
	 **/
	@ApiModelProperty(example = ".*[eE]rror.*|.*[eE]xcepti[o?]n.*", value = "Regular expression to be applied as Oracles to find suspicious and erroneous messages.")

	public String getSuspiciousTitles() {
		return suspiciousTitles;
	}

	public void setSuspiciousTitles(String suspiciousTitles) {
		this.suspiciousTitles = suspiciousTitles;
	}

	public Settings timeToFreeze(BigDecimal timeToFreeze) {
		this.timeToFreeze = timeToFreeze;
		return this;
	}

	/**
	 * Time to check if the SUT do not respond to our interaction and consider it freezes.
	 * minimum: 30
	 * maximum: 120
	 * @return timeToFreeze
	 **/
	@ApiModelProperty(example = "30", value = "Time to check if the SUT do not respond to our interaction and consider it freezes.")

	@Valid
	@DecimalMin("30") @DecimalMax("120")   public BigDecimal getTimeToFreeze() {
		return timeToFreeze;
	}

	public void setTimeToFreeze(BigDecimal timeToFreeze) {
		this.timeToFreeze = timeToFreeze;
	}

	public Settings processListenerEnabled(Boolean processListenerEnabled) {
		this.processListenerEnabled = processListenerEnabled;
		return this;
	}

	/**
	 * Enable reading the output and error buffer of the SUT to apply Oracles at process buffer level. (Only available for desktop applications through COMMAND_LINE).
	 * @return processListenerEnabled
	 **/
	@ApiModelProperty(example = "false", value = "Enable reading the output and error buffer of the SUT to apply Oracles at process buffer level. (Only available for desktop applications through COMMAND_LINE).")

	public Boolean isProcessListenerEnabled() {
		return processListenerEnabled;
	}

	public void setProcessListenerEnabled(Boolean processListenerEnabled) {
		this.processListenerEnabled = processListenerEnabled;
	}

	public Settings suspiciousProcessOutput(String suspiciousProcessOutput) {
		this.suspiciousProcessOutput = suspiciousProcessOutput;
		return this;
	}

	/**
	 * Regular expression to be applied as Oracles to find suspicious and erroneous messages at process buffer level. (Only available for desktop applications through COMMAND_LINE).
	 * @return suspiciousProcessOutput
	 **/
	@ApiModelProperty(example = ".*[eE]rror.*|.*[eE]xcepti[o?]n.*", value = "Regular expression to be applied as Oracles to find suspicious and erroneous messages at process buffer level. (Only available for desktop applications through COMMAND_LINE).")

	public String getSuspiciousProcessOutput() {
		return suspiciousProcessOutput;
	}

	public void setSuspiciousProcessOutput(String suspiciousProcessOutput) {
		this.suspiciousProcessOutput = suspiciousProcessOutput;
	}

	public Settings processLogs(String processLogs) {
		this.processLogs = processLogs;
		return this;
	}

	/**
	 * Regular expression to be applied at process buffer level to save relevant information on TESTAR logs. (Only available for desktop applications through COMMAND_LINE).
	 * @return processLogs
	 **/
	@ApiModelProperty(example = ".*[eE]rror.*|.*[eE]xcepti[o?]n.*", value = "Regular expression to be applied at process buffer level to save relevant information on TESTAR logs. (Only available for desktop applications through COMMAND_LINE).")

	public String getProcessLogs() {
		return processLogs;
	}

	public void setProcessLogs(String processLogs) {
		this.processLogs = processLogs;
	}

	public Settings clickFilter(String clickFilter) {
		this.clickFilter = clickFilter;
		return this;
	}

	/**
	 * Regular expression to filter widgets we don not want to interact with.
	 * @return clickFilter
	 **/
	@ApiModelProperty(example = ".*[sS]istema.*|.*[sS]ystem.*|.*[cC]errar.*|.*[cC]lose.*|.*[sS]alir.*|.*[eE]xit.*|.*[mM]inimizar.*|.*[mM]inimi[zs]e.*|.*[sS]ave.*|.*[gG]uardar.*|.*[fF]ormat.*|.*[fF]ormatear.*|.*[pP]rint.*|.*[iI]mprimir.*|.*[eE]mail.*|.*[fF]ile.*|.*[aA]rchivo.*|.*[dD]isconnect.*|.*[dD]esconectar.*", value = "Regular expression to filter widgets we don not want to interact with.")

	public String getClickFilter() {
		return clickFilter;
	}

	public void setClickFilter(String clickFilter) {
		this.clickFilter = clickFilter;
	}

	public Settings forceForeground(Boolean forceForeground) {
		this.forceForeground = forceForeground;
		return this;
	}

	/**
	 * Force the SUT to be in the foreground if TESTAR detects that is in the background.
	 * @return forceForeground
	 **/
	@ApiModelProperty(example = "true", value = "Force the SUT to be in the foreground if TESTAR detects that is in the background.")

	public Boolean isForceForeground() {
		return forceForeground;
	}

	public void setForceForeground(Boolean forceForeground) {
		this.forceForeground = forceForeground;
	}

	public Settings actionDuration(BigDecimal actionDuration) {
		this.actionDuration = actionDuration;
		return this;
	}

	/**
	 * How fast TESTAR will move the mouse or press the keyboard to interact with the SUT (seconds).
	 * minimum: 0
	 * @return actionDuration
	 **/
	@ApiModelProperty(example = "0.3", value = "How fast TESTAR will move the mouse or press the keyboard to interact with the SUT (seconds).")

	@Valid
	@DecimalMin("0")  public BigDecimal getActionDuration() {
		return actionDuration;
	}

	public void setActionDuration(BigDecimal actionDuration) {
		this.actionDuration = actionDuration;
	}

	public Settings timeToWaitAfterAction(BigDecimal timeToWaitAfterAction) {
		this.timeToWaitAfterAction = timeToWaitAfterAction;
		return this;
	}

	/**
	 * Time that TESTAR will wait after the execution of an action before continue his flow generation (seconds).
	 * minimum: 0
	 * @return timeToWaitAfterAction
	 **/
	@ApiModelProperty(example = "0.3", value = "Time that TESTAR will wait after the execution of an action before continue his flow generation (seconds).")

	@Valid
	@DecimalMin("0")  public BigDecimal getTimeToWaitAfterAction() {
		return timeToWaitAfterAction;
	}

	public void setTimeToWaitAfterAction(BigDecimal timeToWaitAfterAction) {
		this.timeToWaitAfterAction = timeToWaitAfterAction;
	}

	public Settings maxTime(BigDecimal maxTime) {
		this.maxTime = maxTime;
		return this;
	}

	/**
	 * Maximum time that a TESTAR sequence can take. After this time not more action will be executed (seconds).
	 * minimum: 600
	 * @return maxTime
	 **/
	@ApiModelProperty(example = "31536000", value = "Maximum time that a TESTAR sequence can take. After this time not more action will be executed (seconds).")

	@Valid
	@DecimalMin("600")  public BigDecimal getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(BigDecimal maxTime) {
		this.maxTime = maxTime;
	}

	public Settings startupTime(BigDecimal startupTime) {
		this.startupTime = startupTime;
		return this;
	}

	/**
	 * Maximum time that TESTAR waits for the SUT to load (seconds). Once the SUT UI is ready, TESTAR will start the test sequence.
	 * minimum: 2
	 * @return startupTime
	 **/
	@ApiModelProperty(example = "5", value = "Maximum time that TESTAR waits for the SUT to load (seconds). Once the SUT UI is ready, TESTAR will start the test sequence.")

	@Valid
	@DecimalMin("2")  public BigDecimal getStartupTime() {
		return startupTime;
	}

	public void setStartupTime(BigDecimal startupTime) {
		this.startupTime = startupTime;
	}

	public Settings maxReward(BigDecimal maxReward) {
		this.maxReward = maxReward;
		return this;
	}

	/**
	 * Maximum Reward for Reinforecement Learning action selection algorithms.
	 * @return maxReward
	 **/
	@ApiModelProperty(example = "99999", value = "Maximum Reward for Reinforecement Learning action selection algorithms.")

	@Valid
	public BigDecimal getMaxReward() {
		return maxReward;
	}

	public void setMaxReward(BigDecimal maxReward) {
		this.maxReward = maxReward;
	}

	public Settings discount(BigDecimal discount) {
		this.discount = discount;
		return this;
	}

	/**
	 * Discount Reward to be applied in Reinforcement Learning action selection algorithms.
	 * @return discount
	 **/
	@ApiModelProperty(example = "0.95", value = "Discount Reward to be applied in Reinforcement Learning action selection algorithms.")

	@Valid
	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public Settings alwaysCompile(Boolean alwaysCompile) {
		this.alwaysCompile = alwaysCompile;
		return this;
	}

	/**
	 * Compiles always the selected Java protocol before start the TESTAR execution.
	 * @return alwaysCompile
	 **/
	@ApiModelProperty(example = "true", value = "Compiles always the selected Java protocol before start the TESTAR execution.")

	public Boolean isAlwaysCompile() {
		return alwaysCompile;
	}

	public void setAlwaysCompile(Boolean alwaysCompile) {
		this.alwaysCompile = alwaysCompile;
	}

	public Settings onlySaveFaultySequences(Boolean onlySaveFaultySequences) {
		this.onlySaveFaultySequences = onlySaveFaultySequences;
		return this;
	}

	/**
	 * Save only TESTAR sequence that contains fails or warnings.
	 * @return onlySaveFaultySequences
	 **/
	@ApiModelProperty(example = "false", value = "Save only TESTAR sequence that contains fails or warnings.")

	public Boolean isOnlySaveFaultySequences() {
		return onlySaveFaultySequences;
	}

	public void setOnlySaveFaultySequences(Boolean onlySaveFaultySequences) {
		this.onlySaveFaultySequences = onlySaveFaultySequences;
	}

	public Settings stopGenerationOnFault(Boolean stopGenerationOnFault) {
		this.stopGenerationOnFault = stopGenerationOnFault;
		return this;
	}

	/**
	 * Stop the TESTAR sequence if an error is detected or continue until all SequenceLength actions are executed.
	 * @return stopGenerationOnFault
	 **/
	@ApiModelProperty(example = "false", value = "Stop the TESTAR sequence if an error is detected or continue until all SequenceLength actions are executed.")

	public Boolean isStopGenerationOnFault() {
		return stopGenerationOnFault;
	}

	public void setStopGenerationOnFault(Boolean stopGenerationOnFault) {
		this.stopGenerationOnFault = stopGenerationOnFault;
	}

	public Settings processesToKillDuringTest(String processesToKillDuringTest) {
		this.processesToKillDuringTest = processesToKillDuringTest;
		return this;
	}

	/**
	 * Processes to kill during TESTAR sequences execution. In case some SUT launch an external an undesired process.
	 * @return processesToKillDuringTest
	 **/
	@ApiModelProperty(value = "Processes to kill during TESTAR sequences execution. In case some SUT launch an external an undesired process.")

	public String getProcessesToKillDuringTest() {
		return processesToKillDuringTest;
	}

	public void setProcessesToKillDuringTest(String processesToKillDuringTest) {
		this.processesToKillDuringTest = processesToKillDuringTest;
	}

	public Settings pathToReplaySequence(String pathToReplaySequence) {
		this.pathToReplaySequence = pathToReplaySequence;
		return this;
	}

	/**
	 * Path to a directory that contains a sequence generated by TESTAR previously, to repeat same actions.
	 * @return pathToReplaySequence
	 **/
	@ApiModelProperty(example = "C:\\Users\\testar\\output\\sequences\\sequence1.testar", value = "Path to a directory that contains a sequence generated by TESTAR previously, to repeat same actions.")

	public String getPathToReplaySequence() {
		return pathToReplaySequence;
	}

	public void setPathToReplaySequence(String pathToReplaySequence) {
		this.pathToReplaySequence = pathToReplaySequence;
	}

	public Settings outputDir(String outputDir) {
		this.outputDir = outputDir;
		return this;
	}

	/**
	 * Select the system directory to save TESTAR output results.
	 * @return outputDir
	 **/
	@ApiModelProperty(example = "output", value = "Select the system directory to save TESTAR output results.")

	public String getOutputDir() {
		return outputDir;
	}

	public void setOutputDir(String outputDir) {
		this.outputDir = outputDir;
	}

	public Settings tempDir(String tempDir) {
		this.tempDir = tempDir;
		return this;
	}

	/**
	 * Select the system directory to save files temporally.
	 * @return tempDir
	 **/
	@ApiModelProperty(example = "output/temp", value = "Select the system directory to save files temporally.")

	public String getTempDir() {
		return tempDir;
	}

	public void setTempDir(String tempDir) {
		this.tempDir = tempDir;
	}

	public Settings myClassPath(String myClassPath) {
		this.myClassPath = myClassPath;
		return this;
	}

	/**
	 * Indicate the settings directory that contains TESTAR compiled java class protocols.
	 * @return myClassPath
	 **/
	@ApiModelProperty(example = "settings", value = "Indicate the settings directory that contains TESTAR compiled java class protocols.")

	public String getMyClassPath() {
		return myClassPath;
	}

	public void setMyClassPath(String myClassPath) {
		this.myClassPath = myClassPath;
	}

	public Settings overrideWebDriverDisplayScale(BigDecimal overrideWebDriverDisplayScale) {
		this.overrideWebDriverDisplayScale = overrideWebDriverDisplayScale;
		return this;
	}

	/**
	 * Sometimes system screen display options affects action coordinates. Overrides the displayscale obtained from the system to solve issues with actions coordinates.
	 * @return overrideWebDriverDisplayScale
	 **/
	@ApiModelProperty(example = "1", value = "Sometimes system screen display options affects action coordinates. Overrides the displayscale obtained from the system to solve issues with actions coordinates.")

	@Valid
	public BigDecimal getOverrideWebDriverDisplayScale() {
		return overrideWebDriverDisplayScale;
	}

	public void setOverrideWebDriverDisplayScale(BigDecimal overrideWebDriverDisplayScale) {
		this.overrideWebDriverDisplayScale = overrideWebDriverDisplayScale;
	}

	public Settings stateModelEnabled(Boolean stateModelEnabled) {
		this.stateModelEnabled = stateModelEnabled;
		return this;
	}

	/**
	 * Enable the use of TESTAR State Model functionality.
	 * @return stateModelEnabled
	 **/
	@ApiModelProperty(example = "true", value = "Enable the use of TESTAR State Model functionality.")

	public Boolean isStateModelEnabled() {
		return stateModelEnabled;
	}

	public void setStateModelEnabled(Boolean stateModelEnabled) {
		this.stateModelEnabled = stateModelEnabled;
	}

	public Settings dataStore(DataStoreEnum dataStore) {
		this.dataStore = dataStore;
		return this;
	}

	/**
	 * Indicate the graph database where are going to use.
	 * @return dataStore
	 **/
	@ApiModelProperty(example = "OrientDB", value = "Indicate the graph database where are going to use.")

	public DataStoreEnum getDataStore() {
		return dataStore;
	}

	public void setDataStore(DataStoreEnum dataStore) {
		this.dataStore = dataStore;
	}

	public Settings dataStoreType(DataStoreTypeEnum dataStoreType) {
		this.dataStoreType = dataStoreType;
		return this;
	}

	/**
	 * Indicate the type of connection with the graph database. Remote or local.
	 * @return dataStoreType
	 **/
	@ApiModelProperty(example = "remote", value = "Indicate the type of connection with the graph database. Remote or local.")

	public DataStoreTypeEnum getDataStoreType() {
		return dataStoreType;
	}

	public void setDataStoreType(DataStoreTypeEnum dataStoreType) {
		this.dataStoreType = dataStoreType;
	}

	public Settings dataStoreServer(String dataStoreServer) {
		this.dataStoreServer = dataStoreServer;
		return this;
	}

	/**
	 * Indicate the remote address of the graph database.
	 * @return dataStoreServer
	 **/
	@ApiModelProperty(example = "10.101.0.100", value = "Indicate the remote address of the graph database.")

	public String getDataStoreServer() {
		return dataStoreServer;
	}

	public void setDataStoreServer(String dataStoreServer) {
		this.dataStoreServer = dataStoreServer;
	}

	public Settings dataStoreDirectory(String dataStoreDirectory) {
		this.dataStoreDirectory = dataStoreDirectory;
		return this;
	}

	/**
	 * Indicate the directory that contains the plocal graph database.
	 * @return dataStoreDirectory
	 **/
	@ApiModelProperty(example = "C:\\Users\\testar\\Desktop\\orientdb-3.0.34\\databases", value = "Indicate the directory that contains the plocal graph database.")

	public String getDataStoreDirectory() {
		return dataStoreDirectory;
	}

	public void setDataStoreDirectory(String dataStoreDirectory) {
		this.dataStoreDirectory = dataStoreDirectory;
	}

	public Settings dataStoreDB(String dataStoreDB) {
		this.dataStoreDB = dataStoreDB;
		return this;
	}

	/**
	 * The name of the database to generate the TESTAR State Model.
	 * @return dataStoreDB
	 **/
	@ApiModelProperty(example = "MyThaiStar", value = "The name of the database to generate the TESTAR State Model.")

	public String getDataStoreDB() {
		return dataStoreDB;
	}

	public void setDataStoreDB(String dataStoreDB) {
		this.dataStoreDB = dataStoreDB;
	}

	public Settings dataStoreUser(String dataStoreUser) {
		this.dataStoreUser = dataStoreUser;
		return this;
	}

	/**
	 * Existing User in the graph database with admin permissions.
	 * @return dataStoreUser
	 **/
	@ApiModelProperty(example = "testar", value = "Existing User in the graph database with admin permissions.")

	public String getDataStoreUser() {
		return dataStoreUser;
	}

	public void setDataStoreUser(String dataStoreUser) {
		this.dataStoreUser = dataStoreUser;
	}

	public Settings dataStorePassword(String dataStorePassword) {
		this.dataStorePassword = dataStorePassword;
		return this;
	}

	/**
	 * Password credentials that uses the previous User.
	 * @return dataStorePassword
	 **/
	@ApiModelProperty(example = "testar", value = "Password credentials that uses the previous User.")

	public String getDataStorePassword() {
		return dataStorePassword;
	}

	public void setDataStorePassword(String dataStorePassword) {
		this.dataStorePassword = dataStorePassword;
	}

	public Settings dataStoreMode(DataStoreModeEnum dataStoreMode) {
		this.dataStoreMode = dataStoreMode;
		return this;
	}

	/**
	 * Select how the data will be stored in the database.
	 * @return dataStoreMode
	 **/
	@ApiModelProperty(example = "instant", value = "Select how the data will be stored in the database.")

	public DataStoreModeEnum getDataStoreMode() {
		return dataStoreMode;
	}

	public void setDataStoreMode(DataStoreModeEnum dataStoreMode) {
		this.dataStoreMode = dataStoreMode;
	}

	public Settings resetDataStore(Boolean resetDataStore) {
		this.resetDataStore = resetDataStore;
		return this;
	}

	/**
	 * WARNING, This DROP all existing content in the database before create the new TESTAR State Model with the new sequence.
	 * @return resetDataStore
	 **/
	@ApiModelProperty(example = "false", value = "WARNING, This DROP all existing content in the database before create the new TESTAR State Model with the new sequence.")

	public Boolean isResetDataStore() {
		return resetDataStore;
	}

	public void setResetDataStore(Boolean resetDataStore) {
		this.resetDataStore = resetDataStore;
	}

	public Settings actionSelectionAlgorithm(ActionSelectionAlgorithmEnum actionSelectionAlgorithm) {
		this.actionSelectionAlgorithm = actionSelectionAlgorithm;
		return this;
	}

	/**
	 * State Model action selection algorithm. If model is not deterministic TESTAR will alternate to random selection.
	 * @return actionSelectionAlgorithm
	 **/
	@ApiModelProperty(example = "unvisited", value = "State Model action selection algorithm. If model is not deterministic TESTAR will alternate to random selection.")

	public ActionSelectionAlgorithmEnum getActionSelectionAlgorithm() {
		return actionSelectionAlgorithm;
	}

	public void setActionSelectionAlgorithm(ActionSelectionAlgorithmEnum actionSelectionAlgorithm) {
		this.actionSelectionAlgorithm = actionSelectionAlgorithm;
	}

	public Settings stateModelStoreWidgets(Boolean stateModelStoreWidgets) {
		this.stateModelStoreWidgets = stateModelStoreWidgets;
		return this;
	}

	/**
	 * Indicate if we want to store the widget tree everytime a new Concrete State is discovered (Higher memory consumption, slower sequences).
	 * @return stateModelStoreWidgets
	 **/
	@ApiModelProperty(example = "false", value = "Indicate if we want to store the widget tree everytime a new Concrete State is discovered (Higher memory consumption, slower sequences).")

	public Boolean isStateModelStoreWidgets() {
		return stateModelStoreWidgets;
	}

	public void setStateModelStoreWidgets(Boolean stateModelStoreWidgets) {
		this.stateModelStoreWidgets = stateModelStoreWidgets;
	}

	public Settings abstractStateAttributes(String abstractStateAttributes) {
		this.abstractStateAttributes = abstractStateAttributes;
		return this;
	}

	/**
	 * Specify the widget attributes that you want to use in constructing the widget and state abstract hash strings. Use a comma separated list.
	 * @return abstractStateAttributes
	 **/
	@ApiModelProperty(example = "WebWidgetIsOffScreen,WebWidgetHref,WebWidgetTagName,WebWidgetTextContent", value = "Specify the widget attributes that you want to use in constructing the widget and state abstract hash strings. Use a comma separated list.")

	public String getAbstractStateAttributes() {
		return abstractStateAttributes;
	}

	public void setAbstractStateAttributes(String abstractStateAttributes) {
		this.abstractStateAttributes = abstractStateAttributes;
	}

	public Settings applicationName(String applicationName) {
		this.applicationName = applicationName;
		return this;
	}

	/**
	 * Use a name identifier to represent the current application. This will be used to create the output structure reports and for State Model purposes.
	 * @return applicationName
	 **/
	@ApiModelProperty(example = "MyThaiStar", value = "Use a name identifier to represent the current application. This will be used to create the output structure reports and for State Model purposes.")

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public Settings applicationVersion(BigDecimal applicationVersion) {
		this.applicationVersion = applicationVersion;
		return this;
	}

	/**
	 * Use a version identifier to represent the current application. This will be used to create the output structure reports and for State Model purposes.
	 * @return applicationVersion
	 **/
	@ApiModelProperty(example = "1.2", value = "Use a version identifier to represent the current application. This will be used to create the output structure reports and for State Model purposes.")

	@Valid
	public BigDecimal getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(BigDecimal applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	public Settings previousApplicationName(String previousApplicationName) {
		this.previousApplicationName = previousApplicationName;
		return this;
	}

	/**
	 * Name identifier that indicates which one is the previous name of the application that acts as SUT. This will be used for State Model Difference purposes.
	 * @return previousApplicationName
	 **/
	@ApiModelProperty(example = "MyThaiStar", value = "Name identifier that indicates which one is the previous name of the application that acts as SUT. This will be used for State Model Difference purposes.")

	public String getPreviousApplicationName() {
		return previousApplicationName;
	}

	public void setPreviousApplicationName(String previousApplicationName) {
		this.previousApplicationName = previousApplicationName;
	}

	public Settings previousApplicationVersion(BigDecimal previousApplicationVersion) {
		this.previousApplicationVersion = previousApplicationVersion;
		return this;
	}

	/**
	 * Version identifier that indicates which one is the previous version of the application that acts as SUT. This will be used for State Model Difference purposes.
	 * @return previousApplicationVersion
	 **/
	@ApiModelProperty(example = "1.1", value = "Version identifier that indicates which one is the previous version of the application that acts as SUT. This will be used for State Model Difference purposes.")

	@Valid
	public BigDecimal getPreviousApplicationVersion() {
		return previousApplicationVersion;
	}

	public void setPreviousApplicationVersion(BigDecimal previousApplicationVersion) {
		this.previousApplicationVersion = previousApplicationVersion;
	}

	public Settings stateModelDifferenceAutomaticReport(Boolean stateModelDifferenceAutomaticReport) {
		this.stateModelDifferenceAutomaticReport = stateModelDifferenceAutomaticReport;
		return this;
	}

	/**
	 * Try to automatically create the State Model Difference report at the end of the current TESTAR execution.
	 * @return stateModelDifferenceAutomaticReport
	 **/
	@ApiModelProperty(example = "true", value = "Try to automatically create the State Model Difference report at the end of the current TESTAR execution.")

	public Boolean isStateModelDifferenceAutomaticReport() {
		return stateModelDifferenceAutomaticReport;
	}

	public void setStateModelDifferenceAutomaticReport(Boolean stateModelDifferenceAutomaticReport) {
		this.stateModelDifferenceAutomaticReport = stateModelDifferenceAutomaticReport;
	}

	public Settings pkMaddress(String pkMaddress) {
		this.pkMaddress = pkMaddress;
		return this;
	}

	/**
	 * IP address of the server that contains the PKM
	 * @return pkMaddress
	 **/
	@ApiModelProperty(example = "10.100.0.200", value = "IP address of the server that contains the PKM")

	public String getPkMaddress() {
		return pkMaddress;
	}

	public void setPkMaddress(String pkMaddress) {
		this.pkMaddress = pkMaddress;
	}

	public Settings pkMport(String pkMport) {
		this.pkMport = pkMport;
		return this;
	}

	/**
	 * Port of the PKM-API
	 * @return pkMport
	 **/
	@ApiModelProperty(example = "8080", value = "Port of the PKM-API")

	public String getPkMport() {
		return pkMport;
	}

	public void setPkMport(String pkMport) {
		this.pkMport = pkMport;
	}

	public Settings pkMdatabase(String pkMdatabase) {
		this.pkMdatabase = pkMdatabase;
		return this;
	}

	/**
	 * PKM project to store TESTAR Artefacts
	 * @return pkMdatabase
	 **/
	@ApiModelProperty(example = "myproject", value = "PKM project to store TESTAR Artefacts")

	public String getPkMdatabase() {
		return pkMdatabase;
	}

	public void setPkMdatabase(String pkMdatabase) {
		this.pkMdatabase = pkMdatabase;
	}

	public Settings pkMusername(String pkMusername) {
		this.pkMusername = pkMusername;
		return this;
	}

	/**
	 * User with permissions to add Artefacts in the PKM
	 * @return pkMusername
	 **/
	@ApiModelProperty(example = "garfield", value = "User with permissions to add Artefacts in the PKM")

	public String getPkMusername() {
		return pkMusername;
	}

	public void setPkMusername(String pkMusername) {
		this.pkMusername = pkMusername;
	}

	public Settings pkMkey(String pkMkey) {
		this.pkMkey = pkMkey;
		return this;
	}

	/**
	 * Key or password for authentication
	 * @return pkMkey
	 **/
	@ApiModelProperty(example = "5cCI6IkpXVCJ9.eyJ1c2VyX25hbWU", value = "Key or password for authentication")

	public String getPkMkey() {
		return pkMkey;
	}

	public void setPkMkey(String pkMkey) {
		this.pkMkey = pkMkey;
	}


	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Settings settings = (Settings) o;
		return Objects.equals(this.sse, settings.sse) &&
				Objects.equals(this.showVisualSettingsDialogOnStartup, settings.showVisualSettingsDialogOnStartup) &&
				Objects.equals(this.mode, settings.mode) &&
				Objects.equals(this.accessBridgeEnabled, settings.accessBridgeEnabled) &&
				Objects.equals(this.suTConnector, settings.suTConnector) &&
				Objects.equals(this.suTConnectorValue, settings.suTConnectorValue) &&
				Objects.equals(this.sequences, settings.sequences) &&
				Objects.equals(this.sequenceLength, settings.sequenceLength) &&
				Objects.equals(this.suspiciousTitles, settings.suspiciousTitles) &&
				Objects.equals(this.timeToFreeze, settings.timeToFreeze) &&
				Objects.equals(this.processListenerEnabled, settings.processListenerEnabled) &&
				Objects.equals(this.suspiciousProcessOutput, settings.suspiciousProcessOutput) &&
				Objects.equals(this.processLogs, settings.processLogs) &&
				Objects.equals(this.clickFilter, settings.clickFilter) &&
				Objects.equals(this.forceForeground, settings.forceForeground) &&
				Objects.equals(this.actionDuration, settings.actionDuration) &&
				Objects.equals(this.timeToWaitAfterAction, settings.timeToWaitAfterAction) &&
				Objects.equals(this.maxTime, settings.maxTime) &&
				Objects.equals(this.startupTime, settings.startupTime) &&
				Objects.equals(this.maxReward, settings.maxReward) &&
				Objects.equals(this.discount, settings.discount) &&
				Objects.equals(this.alwaysCompile, settings.alwaysCompile) &&
				Objects.equals(this.onlySaveFaultySequences, settings.onlySaveFaultySequences) &&
				Objects.equals(this.stopGenerationOnFault, settings.stopGenerationOnFault) &&
				Objects.equals(this.processesToKillDuringTest, settings.processesToKillDuringTest) &&
				Objects.equals(this.pathToReplaySequence, settings.pathToReplaySequence) &&
				Objects.equals(this.outputDir, settings.outputDir) &&
				Objects.equals(this.tempDir, settings.tempDir) &&
				Objects.equals(this.myClassPath, settings.myClassPath) &&
				Objects.equals(this.overrideWebDriverDisplayScale, settings.overrideWebDriverDisplayScale) &&
				Objects.equals(this.stateModelEnabled, settings.stateModelEnabled) &&
				Objects.equals(this.dataStore, settings.dataStore) &&
				Objects.equals(this.dataStoreType, settings.dataStoreType) &&
				Objects.equals(this.dataStoreServer, settings.dataStoreServer) &&
				Objects.equals(this.dataStoreDirectory, settings.dataStoreDirectory) &&
				Objects.equals(this.dataStoreDB, settings.dataStoreDB) &&
				Objects.equals(this.dataStoreUser, settings.dataStoreUser) &&
				Objects.equals(this.dataStorePassword, settings.dataStorePassword) &&
				Objects.equals(this.dataStoreMode, settings.dataStoreMode) &&
				Objects.equals(this.resetDataStore, settings.resetDataStore) &&
				Objects.equals(this.actionSelectionAlgorithm, settings.actionSelectionAlgorithm) &&
				Objects.equals(this.stateModelStoreWidgets, settings.stateModelStoreWidgets) &&
				Objects.equals(this.abstractStateAttributes, settings.abstractStateAttributes) &&
				Objects.equals(this.applicationName, settings.applicationName) &&
				Objects.equals(this.applicationVersion, settings.applicationVersion) &&
				Objects.equals(this.previousApplicationName, settings.previousApplicationName) &&
				Objects.equals(this.previousApplicationVersion, settings.previousApplicationVersion) &&
				Objects.equals(this.stateModelDifferenceAutomaticReport, settings.stateModelDifferenceAutomaticReport) &&
				Objects.equals(this.pkMaddress, settings.pkMaddress) &&
				Objects.equals(this.pkMport, settings.pkMport) &&
				Objects.equals(this.pkMdatabase, settings.pkMdatabase) &&
				Objects.equals(this.pkMusername, settings.pkMusername) &&
				Objects.equals(this.pkMkey, settings.pkMkey);
	}

	@Override
	public int hashCode() {
		return Objects.hash(sse, showVisualSettingsDialogOnStartup, mode, accessBridgeEnabled, suTConnector, suTConnectorValue, sequences, sequenceLength, suspiciousTitles, timeToFreeze, processListenerEnabled, suspiciousProcessOutput, processLogs, clickFilter, forceForeground, actionDuration, timeToWaitAfterAction, maxTime, startupTime, maxReward, discount, alwaysCompile, onlySaveFaultySequences, stopGenerationOnFault, processesToKillDuringTest, pathToReplaySequence, outputDir, tempDir, myClassPath, overrideWebDriverDisplayScale, stateModelEnabled, dataStore, dataStoreType, dataStoreServer, dataStoreDirectory, dataStoreDB, dataStoreUser, dataStorePassword, dataStoreMode, resetDataStore, actionSelectionAlgorithm, stateModelStoreWidgets, abstractStateAttributes, applicationName, applicationVersion, previousApplicationName, previousApplicationVersion, stateModelDifferenceAutomaticReport, pkMaddress, pkMport, pkMdatabase, pkMusername, pkMkey);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Settings {\n");

		sb.append("    sse: ").append(toIndentedString(sse)).append("\n");
		sb.append("    showVisualSettingsDialogOnStartup: ").append(toIndentedString(showVisualSettingsDialogOnStartup)).append("\n");
		sb.append("    mode: ").append(toIndentedString(mode)).append("\n");
		sb.append("    accessBridgeEnabled: ").append(toIndentedString(accessBridgeEnabled)).append("\n");
		sb.append("    suTConnector: ").append(toIndentedString(suTConnector)).append("\n");
		sb.append("    suTConnectorValue: ").append(toIndentedString(suTConnectorValue)).append("\n");
		sb.append("    sequences: ").append(toIndentedString(sequences)).append("\n");
		sb.append("    sequenceLength: ").append(toIndentedString(sequenceLength)).append("\n");
		sb.append("    suspiciousTitles: ").append(toIndentedString(suspiciousTitles)).append("\n");
		sb.append("    timeToFreeze: ").append(toIndentedString(timeToFreeze)).append("\n");
		sb.append("    processListenerEnabled: ").append(toIndentedString(processListenerEnabled)).append("\n");
		sb.append("    suspiciousProcessOutput: ").append(toIndentedString(suspiciousProcessOutput)).append("\n");
		sb.append("    processLogs: ").append(toIndentedString(processLogs)).append("\n");
		sb.append("    clickFilter: ").append(toIndentedString(clickFilter)).append("\n");
		sb.append("    forceForeground: ").append(toIndentedString(forceForeground)).append("\n");
		sb.append("    actionDuration: ").append(toIndentedString(actionDuration)).append("\n");
		sb.append("    timeToWaitAfterAction: ").append(toIndentedString(timeToWaitAfterAction)).append("\n");
		sb.append("    maxTime: ").append(toIndentedString(maxTime)).append("\n");
		sb.append("    startupTime: ").append(toIndentedString(startupTime)).append("\n");
		sb.append("    maxReward: ").append(toIndentedString(maxReward)).append("\n");
		sb.append("    discount: ").append(toIndentedString(discount)).append("\n");
		sb.append("    alwaysCompile: ").append(toIndentedString(alwaysCompile)).append("\n");
		sb.append("    onlySaveFaultySequences: ").append(toIndentedString(onlySaveFaultySequences)).append("\n");
		sb.append("    stopGenerationOnFault: ").append(toIndentedString(stopGenerationOnFault)).append("\n");
		sb.append("    processesToKillDuringTest: ").append(toIndentedString(processesToKillDuringTest)).append("\n");
		sb.append("    pathToReplaySequence: ").append(toIndentedString(pathToReplaySequence)).append("\n");
		sb.append("    outputDir: ").append(toIndentedString(outputDir)).append("\n");
		sb.append("    tempDir: ").append(toIndentedString(tempDir)).append("\n");
		sb.append("    myClassPath: ").append(toIndentedString(myClassPath)).append("\n");
		sb.append("    overrideWebDriverDisplayScale: ").append(toIndentedString(overrideWebDriverDisplayScale)).append("\n");
		sb.append("    stateModelEnabled: ").append(toIndentedString(stateModelEnabled)).append("\n");
		sb.append("    dataStore: ").append(toIndentedString(dataStore)).append("\n");
		sb.append("    dataStoreType: ").append(toIndentedString(dataStoreType)).append("\n");
		sb.append("    dataStoreServer: ").append(toIndentedString(dataStoreServer)).append("\n");
		sb.append("    dataStoreDirectory: ").append(toIndentedString(dataStoreDirectory)).append("\n");
		sb.append("    dataStoreDB: ").append(toIndentedString(dataStoreDB)).append("\n");
		sb.append("    dataStoreUser: ").append(toIndentedString(dataStoreUser)).append("\n");
		sb.append("    dataStorePassword: ").append(toIndentedString(dataStorePassword)).append("\n");
		sb.append("    dataStoreMode: ").append(toIndentedString(dataStoreMode)).append("\n");
		sb.append("    resetDataStore: ").append(toIndentedString(resetDataStore)).append("\n");
		sb.append("    actionSelectionAlgorithm: ").append(toIndentedString(actionSelectionAlgorithm)).append("\n");
		sb.append("    stateModelStoreWidgets: ").append(toIndentedString(stateModelStoreWidgets)).append("\n");
		sb.append("    abstractStateAttributes: ").append(toIndentedString(abstractStateAttributes)).append("\n");
		sb.append("    applicationName: ").append(toIndentedString(applicationName)).append("\n");
		sb.append("    applicationVersion: ").append(toIndentedString(applicationVersion)).append("\n");
		sb.append("    previousApplicationName: ").append(toIndentedString(previousApplicationName)).append("\n");
		sb.append("    previousApplicationVersion: ").append(toIndentedString(previousApplicationVersion)).append("\n");
		sb.append("    stateModelDifferenceAutomaticReport: ").append(toIndentedString(stateModelDifferenceAutomaticReport)).append("\n");
		sb.append("    pkMaddress: ").append(toIndentedString(pkMaddress)).append("\n");
		sb.append("    pkMport: ").append(toIndentedString(pkMport)).append("\n");
		sb.append("    pkMdatabase: ").append(toIndentedString(pkMdatabase)).append("\n");
		sb.append("    pkMusername: ").append(toIndentedString(pkMusername)).append("\n");
		sb.append("    pkMkey: ").append(toIndentedString(pkMkey)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
