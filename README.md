TESTAR tool DECODER version - Manual
====================================

TESTAR distributed files
------------------------

branch: master

TESTAR Development branch
-------------------------

https://github.com/TESTARtool/TESTAR_dev/tree/decoder_master

See HandsOn manual at the end.

Installation
------------

### Compile and prepare TESTAR API

- Move to `testar_tool\testar_client_api`
- Execute `mvn install`
- This will create the TESTAR API file `testar_tool\testar_client_api\target\testar-client-api-1.X.0.jar`
- Move this api.jar from `testar_tool\testar_client_api\target\testar-client-api-1.X.0.jar` to `testar_tool\bin\testar-client-api-1.X.0.jar`
- From directory `testar_tool\bin` execute `java -jar testar-client-api-1.X.0.jar`

### Environment and tools dependencies for TESTAR

1. Windows 10 OS
2. Java 1.8


### Web applications as MyThaiStar

3.1. Download Selenium Chromedriver version (according to Chrome browser version): https://chromedriver.chromium.org/

3.2. Inside '`C:\\webdrivers\\chromedriver.exe`'


### Java Desktop apps as CODEO with additional JaCoCo report

4.1. Extract Java classfiles in the desired directory

4.2. Prepare or Modify the needed '`build.xml`' file that aims to this classfiles directory. Ex: '`bin\\settings\\desktop_codeo\\build.xml`'


### OrientDB (3.0.34) for State Model storage

5.1. Download OrientDB Community Edition version 3.0.34: https://www.orientdb.org/download

5.1.1.- Windows: https://s3.us-east-2.amazonaws.com/orientdb3/releases/3.0.34/orientdb-3.0.34.zip

5.1.2.- Linux: https://s3.us-east-2.amazonaws.com/orientdb3/releases/3.0.34/orientdb-3.0.34.tar.gz

5.2. Extract all files and execute '`orientdb-3.0.34\bin\server`'

5.3. First time admin credential will be required

5.4. Use the browser to acces to OrientDB management page using port 2480 (by default)

5.5. Create a new database with admin credentials

5.6. Create new user in the Security tab (user '`testar`' + password '`testar`' is used by default in TESTAR settings)

5.7. Now it is possible to connect TESTAR with remote mode to OrientDB with the server IP address

5.8. Visual steps in Slides 44 to 61: https://testar.org/images/development/TESTAR_webdriver_state_model.pdf


LAUNCH TESTAR SERVER
--------------------

### Launch Spring Boot server

1. From '`testar_tool\\bin`'

2. Execute '`java -jar testar-client-api-1.X.0.jar`'


PKM Architecture
----------------

TESTAR creates two Artefacts at the end of its execution: '`ArtefactTestResults`' and '`ArtefactStateModel`'.

Then it launches a curl POST request to the PKM-API TESTAR services ( https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/services/TESTAR ).
To interact with the PKM-API it is necessary to configure the settings:

		{
		  "PKMaddress": "decoder.pkm.api.address",
		  "PKMport": "8080",
		  "PKMdatabase": "myproject",
		  "PKMusername": "garfield",
		  "PKMkey": "5cCI6IkpXVCJ9.eyJ1c2VyX25hbWU.USER.KEY.USER.KEY.USER.KEY"
		}


Example of internal TESTAR curl execution to the PKM-API:

``curl -k -X POST "https://decoder.pkm.api.address:8080/testar/test_results/myproject" -H  "accept: application/json" -H  "key:5cCI6IkpXVCJ9.eyJ1c2VyX25hbWU" -H  "Content-Type: application/json" -d @ArtefactTestResults_MyThaiStar_2020.1_2020-06-15_12h14m24s.json``
		
PKM will validate both Artefacts with the PKM-API schemas ( https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/schemas ) and will return both ArtefactIds if everything was successfully executed.

![Arch TESTAR](images/Arch_TESTAR.png)


TESTAR Settings
---------------

* Settings that TESTAR needs to connect with the SUT, select the protocol, connect with the PKM and OrientDB are necessary.

### Most important settings

``sse : Select a specific TESTAR protocol to run (bin/settings/webdriver_MyThaiStar/Protocol_webdriver_MyThaiStar.java)``

``SUTConnector : Indicate how we want to connect to the SUT``

``SUTConnectorValue : Path to connect with the SUT``

``Sequences : Number of times to start the SUT, execute actions, stop the SUT and start again``

``SequenceLength : Number of actions for each sequence``

``OverrideWebDriverDisplayScale : (Default = 1.0) We recommend to configure the TESTAR environment with a 100% display (no zoom) but if is not possible, this setting Overrides the displayscale obtained from the system to solve problems when the mouse clicks are not aligned with the elements on the screen``

``PKMaddress : Web address that allows us to connect to the PKM-API``

``PKMport : Web port that listens request to the PKM-API``

``PKMdatabase : Project name of the PKM database on which we want to store the generated Artefacts``

``PKMusername : (Deprecated) We don't need the name, just the key``

``PKMkey : Key that identifies a user and hir permissions on PKM-API projects``

``StateModelEnabled : Generate a State Model``

``DataStoreType : How to connect with OrientDB (remote = ip address, plocal = local files)``

``DataStoreServer : IP address to connect with OrientDB``

``DataStoreDB : The name of the OrientDB database to store the State Model``

``DataStoreUser : User with permissions to read and write``

``DataStorePassword : Password credential``

``ResetDataStore : (WARNING) Delete all existing State Models from DataStoreDB before create a new one``

``ActionSelectionAlgorithm : Action selection mechanism to execute actions (Random, Unvisited)``

``StateModelStoreWidgets : Selects if we want to store all widgets for every State. This increases all used resources (storage, memory, time) but saves more information for State Model Difference HTML Report``

``AbstractStateAttributes : Specify the widget attributes that TESTAR uses to identify Abstract Widgets and States. Different attributes create different State Models``

``StateModelDifferenceAutomaticReport : Try to check if previous State Model exists to create the Model Difference HTML report``

``ApplicationName : Custom name to identify the SUT. Different names create different State Models``

``ApplicationVersion : Custom version to identify the SUT. Different versions create different State Models``

``PreviousApplicationName : Name that identifies the previous State Model``

``PreviousApplicationVersion : Version that identifies the previous State Model``

### Other settings

* https://github.com/TESTARtool/TESTAR_dev/wiki/TESTAR-Settings


TESTAR API
----------

### POST Execution to create Artefacts

* MyThaiStar settings example

		{
		"sse": "webdriver_MyThaiStar",
		"SUTConnector": "WEB_DRIVER",
		"SUTConnectorValue": "http://myhtaistar.application:port",
		"Sequences": 1,
		"SequenceLength": 10,
		"OverrideWebDriverDisplayScale": 1,
		"PKMaddress": "decoder.pkm.api.address",
		"PKMport": "8080",
		"PKMdatabase": "demo_project",
		"PKMusername": "demo_user",
		"PKMkey": "USER.KEY.USER.KEY.USER.KEY.USER.KEY",
		"StateModelEnabled": true,
		"DataStore": "OrientDB",
		"DataStoreType": "remote",
		"DataStoreServer": "127.0.0.1",
		"DataStoreDirectory": "C:\\Users\\testar\\Desktop\\orientdb-3.0.34\\databases",
		"DataStoreDB": "MyThaiStar",
		"DataStoreUser": "testar",
		"DataStorePassword": "testar",
		"DataStoreMode": "instant",
		"ResetDataStore": false,
		"ActionSelectionAlgorithm": "unvisited",
		"StateModelStoreWidgets": false,
		"AbstractStateAttributes": "WebWidgetIsOffScreen,WebWidgetHref,WebWidgetTagName,WebWidgetTextContent",
		"ApplicationName": "MyThaiStar",
		"ApplicationVersion": 1,
		"PreviousApplicationName": "MyThaiStar",
		"PreviousApplicationVersion": 0,
		"StateModelDifferenceAutomaticReport": true,
		"ShowVisualSettingsDialogOnStartup": false,
		"Mode": "Generate",
		"AccessBridgeEnabled": false,
		"SuspiciousTitles": ".*[eE]rror.*|.*[eE]xcepti[o?]n.*",
		"TimeToFreeze": 30,
		"ProcessListenerEnabled": false,
		"SuspiciousProcessOutput": ".*[eE]rror.*|.*[eE]xcepti[o?]n.*",
		"ProcessLogs": ".*[eE]rror.*|.*[eE]xcepti[o?]n.*",
		"ClickFilter": ".*[sS]istema.*|.*[sS]ystem.*|.*[cC]errar.*|.*[cC]lose.*|.*[sS]alir.*|.*[eE]xit.*|.*[mM]inimizar.*|.*[mM]inimi[zs]e.*|.*[sS]ave.*|.*[gG]uardar.*|.*[fF]ormat.*|.*[fF]ormatear.*|.*[pP]rint.*|.*[iI]mprimir.*|.*[eE]mail.*|.*[fF]ile.*|.*[aA]rchivo.*|.*[dD]isconnect.*|.*[dD]esconectar.*",
		"ForceForeground": true,
		"ActionDuration": 0.5,
		"TimeToWaitAfterAction": 0.5,
		"MaxTime": 31536000,
		"StartupTime": 5,
		"MaxReward": 99999,
		"Discount": 0.95,
		"AlwaysCompile": true,
		"OnlySaveFaultySequences": false,
		"StopGenerationOnFault": false,
		"ProcessesToKillDuringTest": "",
		"PathToReplaySequence": "C:\\Users\\testar\\output\\sequences\\sequence1.testar",
		"OutputDir": "output",
		"TempDir": "output/temp",
		"MyClassPath": "settings"
		}


### GET HTML report visualization

* By indicating the ArtefactID of the HTML report to review, a web service will be executed by TESTAR.

* Access port 8091 for review. Ex: '`http://localhost:8091/`'

* Shutdown and stop service with shutdown path. Ex: '`http://localhost:8091/shutdown`'

### GET Analysis mode for State Model visualization

* By indicating the OrientDB settings that contains the State Model, a web service will be executed by TESTAR.

* Access port 8090 and path models for review. Ex: '`http://localhost:8090/models/models`'

* Shutdown and stop service with shutdown path. Ex: '`http://localhost:8090/models/shutdown`'


Additional documentation
------------------------

### Development branch
https://github.com/TESTARtool/TESTAR_dev/tree/decoder_master

### TESTAR HandsOn
https://testar.org/images/development/Hands_on_TESTAR_Training_Manual_2020_October_14.pdf

### Webdriver and State Model
https://testar.org/images/development/TESTAR_webdriver_state_model.pdf
